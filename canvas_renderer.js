"use strict";
function CanvasPixel(r = 0, g = 0, b = 0) {
	this.r = r;
	this.g = g
	this.b = b;
}

function CanvasRenderer(canvas_id, canvas_width, canvas_height) {
	this.canvas = document.getElementById(canvas_id);
	this.canvas.width = canvas_width;
	this.canvas.height = canvas_height;
	this.canvas_context = canvas.getContext("2d");
	this.pixels = [];

	for (var i = 0; i < this.canvas.width * this.canvas.height; i += 1) {
		this.pixels[i] = new CanvasPixel(1.0, 0.0, 1.0);
	}
}

CanvasRenderer.prototype.pixelAt = function(x, y) {
	var index = (x + y * this.canvas.width);
	return this.pixels[index];
}

CanvasRenderer.prototype.Render = function() {
	var img_data = this.canvas_context.getImageData(0, 0, this.canvas.width, this.canvas.height);

	for (var i = 0; i < this.canvas.width * this.canvas.height; i += 1) {
		img_data.data[i*4 + 0] = Math.round(this.pixels[i].r * 255);
		img_data.data[i*4 + 1] = Math.round(this.pixels[i].g * 255);
		img_data.data[i*4 + 2] = Math.round(this.pixels[i].b * 255);
		img_data.data[i*4 + 3] = 255;
	}

	this.canvas_context.putImageData(img_data, 0, 0);
}
