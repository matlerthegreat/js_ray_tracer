"use strict"
function Camera(viewport_width, viewport_height, fov) {
	let aspectRatio = viewport_height / viewport_width;

	this.frustum_width = 2 * Math.tan(fov/2);
	this.frustum_height = this.frustum_width * aspectRatio;
	this.pixel_width = this.frustum_width / (viewport_width - 1);
	this.pixel_height = this.frustum_height / (viewport_height - 1);

	this.viewport_width = viewport_width;
	this.viewport_height = viewport_height;

	this.right = new Vector(1, 0, 0);
	this.up = new Vector(0, 1, 0);
	this.front = new Vector(0, 0, 1);
}

Camera.prototype = {
	forEachPixel: function(callback) {
		for (var x = 0; x < this.viewport_width; ++x) {
			for (var y = 0; y < this.viewport_height; ++y) {
				var frustum_x = x * this.pixel_width - 0.5 * this.frustum_width;
				var frustum_y = y * this.pixel_height - 0.5 * this.frustum_height;
				var ray = this.front.clone();
				ray = ray.add((Vector.multiply(this.right, frustum_x)));
				ray = ray.add(Vector.multiply(this.up, frustum_y));
				ray = ray.unit();
				callback(x, y, ray);
			}
		}
	}
}
var canvas_id = "canvas";
var width = 800;
var height = 600;
var fov = 45;
var renderer = new CanvasRenderer(canvas_id, width, height);
var camera = new Camera(width, height, fov/180*Math.PI);
let camera_pos = new Vector(0, 0, -10);
voxel_chunk.GenerateRandomVoxels();

function ShootRay(pixel_x, pixel_y, ray) {
	var pixel = renderer.pixelAt(pixel_x, pixel_y);
	let voxel = voxel_chunk.get_ray_intersection(ray, camera_pos);
	if (voxel) {
		pixel.r = 0;
		pixel.g = 0.5;
		pixel.b = 0.5;
	}
	else {
		pixel.r = 0;
		pixel.g = 0;
		pixel.b = 0;
	}
}

function Render()
{
	renderer.Render();
	camera.forEachPixel(ShootRay);
	renderer.Render();
	/*for (var pixel_x = 0; pixel_x < renderer.canvas.width; pixel_x += 1) {
		for (var pixel_y = 0; pixel_y < renderer.canvas.height; pixel_y += 1) {
			var pixel = renderer.pixelAt(pixel_x, pixel_y);
			pixel.r *= 0.999;
			pixel.g *= 0.999;
			pixel.b *= 0.999;
		}
	}*/

	//window.requestAnimationFrame(Render);
}

window.requestAnimationFrame(Render);
