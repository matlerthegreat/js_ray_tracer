"use strict";

var voxel_chunk = {
	dimension: 32,
	diameter: 32,
	voxels: [],
	position: new Vector(0, 0, 10),
	GenerateRandomVoxels: function() {
		var size = this.dimension*this.dimension*this.dimension;
		for (var i = 0; i < size; i += 1) {
			if (Math.random() < 0.95)
				this.voxels[i] = {element: 0};
			else
				this.voxels[i] = {element: 1};
		}
	},
	GetVoxel: function(x, y, z) {
		let index = (x*this.dimension*this.dimension)+(y*this.dimension)+z;
		return this.voxels[index];
	},
	get_ray_intersection: function(ray_direction, ray_origin) {
		let voxel_diameter = this.diameter / this.dimension;

		// Convert origin to be in respact to current chunk
		let origin = ray_origin.subtract(this.position);
		origin = origin.divide(voxel_diameter);
		origin = origin.add(this.dimension / 2);
		origin = origin.subtract(0.5);

		let voxel_pos = new Vector(Math.round(origin.x), Math.round(origin.y), Math.round(origin.z));
		let offset = origin.subtract(voxel_pos);

		const max_iterations_count = 64;
		for (let i = 0; i < max_iterations_count; ++i) {
			let dx = Math.abs((0.5/ray_direction.x)-(offset.x/Math.abs(ray_direction.x)));
			let dy = Math.abs((0.5/ray_direction.y)-(offset.y/Math.abs(ray_direction.y)));
			let dz = Math.abs((0.5/ray_direction.z)-(offset.z/Math.abs(ray_direction.z)));

			let step = dx;
			let next_voxel = new Vector(Math.sign(ray_direction.x), 0, 0);
			if (dy < step) {
				step = dy;
				next_voxel = new Vector(0, Math.sign(ray_direction.y), 0);
			}
			if (dz < step) {
				step = dz;
				next_voxel = new Vector(0, 0, Math.sign(ray_direction.z));
			}

			offset = offset.add(ray_direction.multiply(step));
			offset = offset.subtract(next_voxel);
			
			voxel_pos = voxel_pos.add(next_voxel);
			if (voxel_pos.x < 0 || voxel_pos.x >= this.dimension)
				continue;
			if (voxel_pos.y < 0 || voxel_pos.y >= this.dimension)
				continue;
			if (voxel_pos.z < 0 || voxel_pos.z >= this.dimension)
				continue;
			let voxel = this.GetVoxel(voxel_pos.x, voxel_pos.y, voxel_pos.z);
			if (voxel.element != 0)
				return voxel;
		}
	}
};

